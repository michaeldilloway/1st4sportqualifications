<?php

// Exit if accessed directly
if( !defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Archive Template
 *
 *
 * @file           archive.php
 * @package        Responsive
 * @author         Emil Uzelac
 * @copyright      2003 - 2014 CyberChimps
 * @license        license.txt
 * @version        Release: 1.1
 * @filesource     wp-content/themes/responsive/archive.php
 * @link           http://codex.wordpress.org/Theme_Development#Archive_.28archive.php.29
 * @since          available since Release 1.0
 */
$type = "";
$area = get_query_var( 'area');
if (get_query_var( 'sport' )){
	$name = get_query_var( 'sport' );
	$type="sportqual";
}else if (get_query_var( 'subject' )){
	$name = get_query_var( 'subject' );
	$type="qualsubject";
}

$term = get_term_by( 'slug', $name , $type);
get_header(); ?>
<?php // 


?>
<div class="menuShow">Menu</div>
<div id="singleContent">
	<?php 
	if ($type=="sportqual"){
		$shortcode = '[wpv-view name="sport-page-banner" sport="' . $term->slug . '" area="' . $area . '"]';
	}else{
		$shortcode = '[wpv-view name="subject-page-banner" subject="' . $term->slug . '" area="' . $area . '"]';
	}
	
	echo do_shortcode($shortcode);
	?>
<?php get_template_part( 'loop-header' ); ?>
	<div id="content-archive" class="post-entry <?php //echo implode( ' ', responsive_get_content_classes() ); ?>">

		<?php //if( have_posts() ) : ?>
			
			<h1><?php echo $term->name; ?></h1>
		<?php 
		if ($type=="sportqual"){
			echo do_shortcode('[wpv-view name="sport-page-content" sport="' . $term->slug . '" area="' . $area . '"]');
		}else{		
			echo do_shortcode('[wpv-view name="subject-page-content" subject="' . $term->slug . '" area="' . $area . '"]');
		}
		if ($area=="centre"){ 
			if ($type=="sportqual"){
				echo do_shortcode('[wpv-view name="qualifications-archive-centre" sport="' . $term->slug . '"]'); 
			}else{	
				echo do_shortcode('[wpv-view name="qualifications-archive-centre" subject="' . $term->slug . '"]'); 			
			}			
		}else{
			if ($type=="sportqual"){
				echo do_shortcode('[wpv-view name="qualifications-archive-learner" sport="' . $term->slug . '"]'); 
			}else{	
				echo do_shortcode('[wpv-view name="qualifications-archive-learner" subject="' . $term->slug . '"]'); 			
			}
			//echo do_shortcode('[wpv-view name="qualifications-archive-learner" sport="' . $term->slug . '"]'); 
		}
		?>
		
		<div class="otherArea">
			<?php if( $area=="centre" ) : ?>
				&gt; Interested in studying? See the <a href="<?php echo getOpositeArchivePage($term->slug, $type, $area)?>">learner versions</a> of these qualifications.
			<?php else : ?>
				&gt; Interested in delivering? See the <a href="<?php echo getOpositeArchivePage($term->slug, $type, $area)?>">centre versions</a> of these qualifications.
			<?php endif; ?>
		</div>
	</div>
</div>
<div id="menu">
<div class="menuHide">
</div>
<?php 
$landingPage = getLandingPage($area);
echo do_shortcode('[wpv-post-body view_template="Left Side Menu" id="' . $landingPage . '"]');  ?> 
 <script type="text/javascript">
	setLandingZone(<?php echo $landingPage ?>);
</script>	
</div>

<?php get_footer(); ?>
