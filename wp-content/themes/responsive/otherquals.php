<?php

// Exit if accessed directly
if( !defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * No-Posts Loop Content Template-Part File
 *
 * @file           otherquals.php
 * @package        Responsive
 * @author         Emil Uzelac
 * @copyright      2003 - 2014 CyberChimps
 * @license        license.txt
 * @version        Release: 1.1.0
 * @filesource     wp-content/themes/responsive/otherquals.php
 * @link           http://codex.wordpress.org/Templates
 * @since          available since Release Mipolla
 */


?>
<?php 
	$qualificationParent = getQualParentID($post->ID); 
	$termsSport = wp_get_post_terms( $qualificationParent, array('sportqual')); 
	$termsSubject = wp_get_post_terms( $qualificationParent, array('qualsubject')); 

	$termsSport_slugs = array();
	foreach ($termsSport as $tag) {
		$termsSport_slugs[] = $tag->slug;
	}

	$termsSubject_slugs = array();
	foreach ($termsSubject as $tag) {
		$termsSubject_slugs[] = $tag->slug;
	}

	$shortcode = "";
	$area = "";
	
	if ($post->post_type == 'learner-qual'){
		$area = "learner";		
	}else{
		$area = "centre";
	}
	$htm = "";
	if (sizeof($termsSubject_slugs) !=0 || sizeof($termsSport_slugs) !=0){
		$shortcode = '[wpv-view name="other-qualifications-' . $area . '" sport="' .  implode(",", $termsSport_slugs )  . '" subject="' .  implode(",", $termsSubject_slugs )   . '" exclude="' . $qualificationParent . '"]';
		$htm= do_shortcode($shortcode);
	}

	$namesString = do_shortcode("[types field='related-qualifications' id='" . $qualificationParent . "' separator=',']");

	if (trim($namesString) != ""){
		$names = explode(",", $namesString);
		$args = array(
			'post_type' => 'qualification',
			'posts_per_page' => 50, 
			'orderby' => 'rand',
			'name__in' => $names
		);
		
		$the_query = new WP_Query( $args );
		
		
		if ( $the_query->have_posts() ) {
			while ( $the_query->have_posts() ){ 
				$the_query->the_post();
				//$qualificationParentPost = getQualParentID($post->ID);
				//echo '[wpv-view name="links-for-' . $area . '-qualifications" parent="'. $post->ID .'"]';
				$htm  .= do_shortcode('[wpv-view name="links-for-' . $area . '-qualifications" parent="'. $post->ID .'"]');
			}
		
		wp_reset_postdata();
		}
	}

 ?>
	

<?php if (trim($htm) != ""): ?>	


<div class="otherQuals">
	<div class="title">Other 1st4sport Qualifications you may be interested in:</div>
	<div class="content">	
		<?php echo $htm;?>
	</div>
</div>

<?php endif; ?>