<?php

// Exit if accessed directly
if( !defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Error 404 Template
 *
 *
 * @file           404.php
 * @package        Responsive
 * @author         Emil Uzelac
 * @copyright      2003 - 2014 CyberChimps
 * @license        license.txt
 * @version        Release: 1.0
 * @filesource     wp-content/themes/responsive/404.php
 * @link           http://codex.wordpress.org/Creating_an_Error_404_Page
 * @since          available since Release 1.0
 */
?>
<?php get_header(); ?>

<div id="content-full" class="grid col-940">

	<?php responsive_entry_before(); ?>
	<div id="post-0" class="error404">
		<?php responsive_entry_top(); ?>

		<div class="post-entry">
		<div class="Title404">
		Sorry that page doesn't exist
		</div>
		<div class="Text404">
			<p>Visit '<a href="<?php echo home_url('/our_qualifications/')?>">Our Qualifications</a>' to find:
			<ul>
				<li>information about all our qualifications whether you are a centre or a learner</li>
				<li>the criteria centres need to meet to become recognised and/or approved to deliver our qualifications</li>
				<li>information about where our qualifications can fit into apprenticeships</li>
			</ul>
			<p>If you are a learner looking to find a centre who delivers any of our qualifications visit the <a href="/candidate_information/find-a-centre">Centre Finder</a></p>
		</div>
		<div class="SubText404">
			You can return &#8592;  <a href="<?php echo home_url('/')?>">Home</a> or search for the page you were looking for.
		</div>
		<div class="Search404">
		<?php get_search_form(); ?>
			
			<?php //get_template_part( 'loop-no-posts' ); ?>
		</div>
		</div><!-- end of .post-entry -->

		<?php responsive_entry_bottom(); ?>
	</div><!-- end of #post-0 -->
	<?php responsive_entry_after(); ?>

</div><!-- end of #content-full -->

<?php get_footer(); ?>
