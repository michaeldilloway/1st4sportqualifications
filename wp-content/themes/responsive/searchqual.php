<?php

// Exit if accessed directly
if( !defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Search Template
 *
 *
 * @file           search.php
 * @package        Responsive
 * @author         Emil Uzelac
 * @copyright      2003 - 2014 CyberChimps
 * @license        license.txt
 * @version        Release: 1.0
 * @filesource     wp-content/themes/responsive/search.php
 * @link           http://codex.wordpress.org/Theme_Development#Search_Results_.28search.php.29
 * @since          available since Release 1.0
 */

get_header(); ?>

<div id="content-search" class="<?php echo implode( ' ', responsive_get_content_classes() ); ?>">
 	<?php 
	$value = get_query_var('code');
	$script = do_shortcode('[wpv-view name="Qualification Search By Code"]');?>
	
	<?php if(trim($script)!="" ) : 
	
		echo $script;
		
	else :

		get_template_part( 'loop-no-quals' );

	endif;
	?>
	

</div><!-- end of #content-search -->

<?php get_footer(); ?>
