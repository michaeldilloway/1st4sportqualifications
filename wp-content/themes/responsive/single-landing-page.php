<?php

// Exit if accessed directly
if( !defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Single Posts Template
 *
 *
 * @file           single.php
 * @package        Responsive
 * @author         Emil Uzelac
 * @copyright      2003 - 2014 CyberChimps
 * @license        license.txt
 * @version        Release: 1.0
 * @filesource     wp-content/themes/responsive/single.php
 * @link           http://codex.wordpress.org/Theme_Development#Single_Post_.28single.php.29
 * @since          available since Release 1.0
 */

get_header(); ?>
<div class="menuShow">Menu</div>
<div id="singleContent">

	<?php echo do_shortcode('[wpv-post-body view_template="page-banner"]');
	?>

	<div id="content" class="<?php //echo implode( ' ', responsive_get_content_classes() ); ?>">
		
		<?php if( have_posts() ) : ?>

			<?php while( have_posts() ) : the_post(); ?>

				<?php responsive_entry_before(); ?>
				<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
				<?php get_template_part( 'loop-header' ); ?> 
					<?php responsive_entry_top(); ?>

					<?php //get_template_part( 'post-meta' ); ?>

					<div class="post-entry">
						<?php echo do_shortcode('[wpv-post-body view_template="right-side-content"]')  ?>
						<?php the_content( __( 'Read more &#8250;', 'responsive' ) ); ?>		
						<div class="responsiveRight">							
						</div>
					</div><!-- end of .post-entry -->

					<?php get_template_part( 'post-data' ); ?>

					<?php responsive_entry_bottom(); ?>
				</div><!-- end of #post-<?php the_ID(); ?> -->

				<?php
			endwhile;

			get_template_part( 'loop-nav' );

		else :

			get_template_part( 'loop-no-posts' );

		endif;
		?>

	</div><!-- end of #content -->
</div>
<div id="menu">
<div class="menuHide">
</div>
<?php $val = $post->ID;
 echo do_shortcode('[wpv-post-body view_template="Left Side Menu"]');  ?> 
 <script type="text/javascript">
	openMenu(<?php echo $post->ID ?>);
</script>
</div>
<?php //get_sidebar(); ?>
<?php get_footer(); ?>
