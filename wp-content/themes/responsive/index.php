<?php

// Exit if accessed directly
if( !defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Index Template
 *
 *
 * @file           index.php
 * @package        Responsive
 * @author         Emil Uzelac
 * @copyright      2003 - 2014 CyberChimps
 * @license        license.txt
 * @version        Release: 1.0
 * @filesource     wp-content/themes/responsive/index.php
 * @link           http://codex.wordpress.org/Theme_Development#Index_.28index.php.29
 * @since          available since Release 1.0
 */

		
get_header();

	
echo do_shortcode('[wpv-view name="Slider"]'); ?> 
<div id="content" >
	<?php if( have_posts() ) : ?>

		<?php while( have_posts() ) : the_post(); ?>

			<?php responsive_entry_before(); ?>
			<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
				<?php responsive_entry_top(); ?>

				<?php get_template_part( 'post-meta-page' ); ?>

				<div class="post-entry">
					<div class="homeLeft">
						<?php the_content( __( 'Read more &#8250;', 'responsive' ) ); ?>
					</div>
					<div class="homeRight">
						<div class="linkSelCont">
							<div class="title">
								You are interested in
							</div>
							<div class="content">
								<?php echo do_shortcode('[wpv-view name="dropdown-links"]');?> 
							</div>
						</div>			
					</div>	
					<div class="clear">
						<?php echo do_shortcode('[wpv-post-body view_template="tabbed-content"]');?>
					</div>
				</div>
				<div class="homeFoot">
				<?php
					echo do_shortcode('[wpv-view name="partner-slider"]');
				?> 
				</div>
				<!-- end of .post-entry -->
				
				<?php get_template_part( 'post-data' ); ?>

				<?php responsive_entry_bottom(); ?>
			</div><!-- end of #post-<?php the_ID(); ?> -->
			<?php responsive_entry_after(); ?>

			<?php //responsive_comments_before(); ?>
			<?php //comments_template( '', true ); ?>
			<?php //responsive_comments_after(); ?>

		<?php
		endwhile;

		get_template_part( 'loop-nav' );

	else :

		//get_template_part( 'loop-no-posts' );

	endif;
	?>

</div><!-- end of #content -->

<?php get_footer(); ?>
