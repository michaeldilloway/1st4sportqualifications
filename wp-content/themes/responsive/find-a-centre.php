<?php

// Exit if accessed directly
if( !defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Pages Template
 *
 *
 * @file           page.php
 * @package        Responsive
 * @author         Emil Uzelac
 * @copyright      2003 - 2014 CyberChimps
 * @license        license.txt
 * @version        Release: 1.0
 * @filesource     wp-content/themes/responsive/page.php
 * @link           http://codex.wordpress.org/Theme_Development#Pages_.28page.php.29
 * @since          available since Release 1.0
 */
 have_posts();
get_header(); 
?>
<div id="finderContent">
<div id="content" class="<?php //echo implode( ' ', responsive_get_content_classes() ); ?>">


				<?php responsive_entry_before(); ?>
				<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					<?php responsive_entry_top(); ?>
					<div class="post-entry">
						<h1>Centre Finder</h1>
						<div class="finderExp">
							<strong>We are currently experiencing technical difficulties with The Recognised Centre Finder. We are working to resolve the issues. </strong> 
						</div>
						<div>
							<div class="finderLeft">
								<div class="selects">
									<div id="loading">
										<img src="<?php echo home_url( '/' ); ?>img/icon/loaderb64.gif"/>
									</div>
									<div class="errorFields">
										<ul>
											<li class="subjectErr">You must select a subject to search for</li>
											<li class="sportErr">You must select a sport to search for</li>
											<li class="addressErr">Please enter a postcode or town</li>
										</ul>
									</div>
									<div class="finderSel">
										<span>Find centres who offer this subject:</span><br/>
										<select id="subject" name="subject"  class="fixedw">
											<option value="" class="empty">Choose your subject...</option>
											<?php echo do_shortcode('[wpv-view name="subjects-select"]') ?>	
										</select>
									</div>
									<div class="finderSel finderSelSport">
										<span>In this sport:</span><br/>
										<select id="sport" name="sport" class="fixedw">
											<option value="" class="empty">Choose your sport...</option>
											<?php echo do_shortcode('[wpv-view name="sports-select"]') ?>	
										</select>
									</div>
									<div class="finderSel">
										<span>Postcode/town: </span>
										<input type="text" name="address" id="address"/>
										<select id="distance" name="distance">
											<option value="5">Within 5 miles</option>
											<option value="10">Within 10 miles</option>
											<option value="25">Within 25 miles</option>
											<option selected="selected" value="50">Within 50 miles</option>
											<option value="100">Within 100 miles</option>
											<option value="-1">Nationwide</option>
										</select>
										<a class="buttonSearch" id="find-a-centre-search">Search</a>
									</div>
								</div>
								
								<div id="finderResults">
								</div>
								
								<div id="noResults">
									There are no 1st4sport recognised centres near you that are approved to deliver the qualification you are looking for. For further details please <a href="/centre-finder-information/">see our information page</a>.
								</div>
							</div>
							<div class="finderRight">
								<div id="mapCanvas">
								</div>
								<div class="finderHelp">
									Don't forget to check the <a href="<?php echo home_url( '/' ); ?>centre-finder-information">list of governing bodies of sport </a> to see which ones run courses nationally as they may not appear in your search results.
								</div>
							</div>
						</div>
					</div><!-- end of .post-entry -->

					<?php get_template_part( 'post-data' ); ?>

					<?php responsive_entry_bottom(); ?>
				</div><!-- end of #post-<?php the_ID(); ?> -->

				<?php

		?>

	</div><!-- end of #content -->
</div>
<?php 
$landingPage = getLandingPage($post->ID);  ?> 
 <script type="text/javascript">
	openMenu(<?php echo $post->ID ?>);
	setLandingZone(<?php echo $landingPage ?>);

</script>	

<?php //get_sidebar(); ?>
<?php get_footer(); ?>