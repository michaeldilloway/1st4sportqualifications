<?php

// Exit if accessed directly
if( !defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Search Template
 *
 *
 * @file           search.php
 * @package        Responsive
 * @author         Emil Uzelac
 * @copyright      2003 - 2014 CyberChimps
 * @license        license.txt
 * @version        Release: 1.0
 * @filesource     wp-content/themes/responsive/search.php
 * @link           http://codex.wordpress.org/Theme_Development#Search_Results_.28search.php.29
 * @since          available since Release 1.0
 */

get_header(); 
?>

<div id="content-search" class="<?php echo implode( ' ', responsive_get_content_classes() ); ?>">
 	<?php if( have_posts() ) : ?>
		<?php get_template_part( 'loop-header' ); ?>

		<?php while( have_posts() ) : the_post(); ?>

			<?php responsive_entry_before(); ?>
			<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			<div class="qualArchiveItem">
			<?php $title =  $post->post_title;
			if ($post->post_type == 'learner-qual'){
				// $title = "(Learner view) " . do_shortcode('[types field="qualification-title" id="$qualification"][/types]');
				$title = "(Learner view) " . $title;
			}else if ($post->post_type == 'centre-qual'){
				// $title = "(Centre view) " . do_shortcode('[types field="qualification-title" id="$parent" debug="true"][/types]');
				$title = "(Centre view) " . $title;
			}
			?>
				<a href="<?php echo get_the_permalink($post->ID) ?>"><?php echo $title ?></a>
			</div>	
			</div><!-- end of #post-<?php the_ID(); ?> -->
			<?php responsive_entry_after(); ?>

		<?php
		endwhile;

		get_template_part( 'loop-nav' );

	else :

		get_template_part( 'loop-no-posts' );

	endif;
	?>

</div><!-- end of #content-search -->

<?php get_footer(); ?>
