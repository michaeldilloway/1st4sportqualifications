<?php

// Exit if accessed directly
if( !defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Pages Template
 *
 *
 * @file           page.php
 * @package        Responsive
 * @author         Emil Uzelac
 * @copyright      2003 - 2014 CyberChimps
 * @license        license.txt
 * @version        Release: 1.0
 * @filesource     wp-content/themes/responsive/page.php
 * @link           http://codex.wordpress.org/Theme_Development#Pages_.28page.php.29
 * @since          available since Release 1.0
 */
 have_posts();
get_header(); 
?>
<div class="menuShow">Menu</div>
<div id="singleContent">

	<?php echo do_shortcode('[wpv-post-body view_template="page-banner"]');
	?>

	<div id="content" class="<?php //echo implode( ' ', responsive_get_content_classes() ); ?>">
		
		<?php if( have_posts() ) : ?>

			<?php while( have_posts() ) : the_post(); ?>

				<?php responsive_entry_before(); ?>
				<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
				<?php get_template_part( 'loop-header' ); ?> 
					<?php responsive_entry_top(); ?>

					<?php //get_template_part( 'post-meta' ); ?>

					<div class="post-entry">
						<?php echo do_shortcode('[wpv-post-body view_template="right-side-content"]')  ?>
							<?php the_content( __( 'Read more &#8250;', 'responsive' ) ); ?>
						<div class="responsiveRight">
						</div>
					</div><!-- end of .post-entry -->

					<?php get_template_part( 'post-data' ); ?>

					<?php responsive_entry_bottom(); ?>
				</div><!-- end of #post-<?php the_ID(); ?> -->

				<?php
			endwhile;

			get_template_part( 'loop-nav' );

		else :

			get_template_part( 'loop-no-posts' );

		endif;
		?>

	</div><!-- end of #content -->
</div>
<div id="menu">
<div class="menuHide">
</div>
<?php 
$landingPage = getLandingPage($post->ID);
echo do_shortcode('[wpv-post-body view_template="Left Side Menu" id="' . $landingPage . '"]');  ?> 
 <script type="text/javascript">
	openMenu(<?php echo $post->ID ?>);
	setLandingZone(<?php echo $landingPage ?>);
	</script>	
</div>
<?php //get_sidebar(); ?>
<?php get_footer(); ?>