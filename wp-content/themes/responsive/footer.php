<?php

// Exit if accessed directly
if( !defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Footer Template
 *
 *
 * @file           footer.php
 * @package        Responsive
 * @author         Emil Uzelac
 * @copyright      2003 - 2014 CyberChimps
 * @license        license.txt
 * @version        Release: 1.2
 * @filesource     wp-content/themes/responsive/footer.php
 * @link           http://codex.wordpress.org/Theme_Development#Footer_.28footer.php.29
 * @since          available since Release 1.0
 */

/*
 * Globalize Theme options
 */
global $responsive_options;
$responsive_options = responsive_get_options();
?>
<?php responsive_wrapper_bottom(); // after wrapper content hook ?>
</div><!-- end of #wrapper -->
<?php responsive_wrapper_end(); // after wrapper hook ?>

<!--|-->
<div id="footer" class="clearfix">
	<?php responsive_footer_top(); ?>

	<div id="footer-wrapper">
	<div id="footerBar">
		<a href="<?php echo home_url('/');?>" class="footerLink">Home</a>
		<a href="<?php echo home_url('/');?>privacystatement/"class="footerLink">Privacy & Cookies</a>
		<a href="<?php echo home_url('/');?>about_us/contact_us/"class="footerLink">Contact Us</a>
		<span class="copyright">
			<?php esc_attr_e( '&copy;', 'responsive' ); ?> <?php echo date( 'Y' ); ?><a href="<?php echo home_url( '/' ) ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>">
				<?php bloginfo( 'name' ); ?>
		</a>
		</span>
	</div>
	<div id="footerFoot">
		<div class="socialIcons">
			<div>
				<a target="_blank" href="https://twitter.com/1st4sportQuals">
					<img src="<?php echo home_url('/');?>img/icon/twitter-icon.png" alt="Twitter"/>
				</a>
			</div>
		</div>
		1st4sport Qualifications is a brand of Coachwise Ltd, the trading arm of UK Coaching (formerly Sports Coach UK), the UK-registered charity leading the development of coaching. Any proceeds from your business goes directly back to UK Coaching to invest in developing coaching for all children, players and athletes in the UK. 
		<br/>
		<br/>
		Coachwise Ltd. is registered in England 02340767, registered offices at: Chelsea Close, Off Amberley Road, Armley, Leeds, LS12 4HP.
		<br/>
		Use of this website constitutes acceptance of the <a href="<?php echo home_url('/');?>privacystatement/">Privacy and Cookies policy</a>. 
	</div>

				
	</div>

	<?php responsive_footer_bottom(); ?>
</div><!-- end #footer -->
</div><!-- end of #container -->
<?php responsive_container_end(); // after container hook ?>
<?php responsive_footer_after(); ?>

<?php wp_footer(); 
$landTitle = "";
if (!is_search() && $post != null){
	$landingPage = getLandingPage($post->ID);
	$postLand = get_post($landingPage);
	$landTitle = $postLand->post_title;
}else if (get_query_var( 'area')){
	$landingPage = getLandingPage($area);
	$postLand = get_post($landingPage);
	$landTitle = $postLand->post_title;
}
?>
 <script type="text/javascript">
	$("body").show();
	$("#responsive_current_menu_item").html('<?php echo $landTitle ?>');
 </script>
</body>
</html>