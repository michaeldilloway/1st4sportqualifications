<?php

// Exit if accessed directly
if( !defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Single Posts Template
 *
 *
 * @file           single.php
 * @package        Responsive
 * @author         Emil Uzelac
 * @copyright      2003 - 2014 CyberChimps
 * @license        license.txt
 * @version        Release: 1.0
 * @filesource     wp-content/themes/responsive/single.php
 * @link           http://codex.wordpress.org/Theme_Development#Single_Post_.28single.php.29
 * @since          available since Release 1.0
 */

get_header(); 
?>
<div class="menuShow">Menu</div>
<div id="singleContent">

	<?php echo do_shortcode('[wpv-post-body view_template="qualification-page-banner" ]');
	?>

	<div id="content" class="<?php //echo implode( ' ', responsive_get_content_classes() ); ?>">
		<?php if( have_posts() ) : ?>

			<?php while( have_posts() ) : the_post(); ?>

				<?php responsive_entry_before(); ?>
				<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
				<?php get_template_part( 'loop-header' ); ?> 
					<?php responsive_entry_top(); ?>

					<?php //get_template_part( 'post-meta' ); ?>

					<div class="post-entry">
						<div class="title">
						<h1><?php echo do_shortcode('[types field="qualification-title" id="$qualification"][/types]')  ?></h1>
						</div>
						<div class="rightHand">
							<?php echo do_shortcode('[types field="right-hand-side-content"][/types]'); ?>
							<br/>
							<div class='staticLink'><a href='<?php echo home_url( '/' ); ?>centre_information/'>Why choose 1st4sport Qualifications?</a></div>
							<div class='staticLink'><a href='<?php echo home_url( '/' ); ?>centre_information/becoming-recognised-centre/'>Become a recognised centre</a></div>
							<div class='staticLink'><a href='<?php echo home_url( '/' ); ?>centre_information/become-approved-to-deliver/'>Deliver this qualification</a></div>
							<?php 
							$parentQual = getQualParentID($post->ID);
							$url = trim(do_shortcode('[wpv-view name="URL for Learner Qualifications" parent="' . $parentQual . '"]')); 
							if ($url != "") : ?>
								<div class="separatorLinks"></div>
								<div class="staticLink"><a href="<?php echo $url ;?>">Learner Information</a></div>
							<?php endif; ?>
						</div>
						<?php echo do_shortcode('[wpv-post-body view_template="None"]')  ?>
						<?php echo get_template_part( 'otherquals' );?>
						<div class="responsiveRight">
						</div>
					</div><!-- end of .post-entry -->

					<?php get_template_part( 'post-data' ); ?>

					<?php responsive_entry_bottom(); ?>
				</div><!-- end of #post-<?php the_ID(); ?> -->

				<?php
			endwhile;

			get_template_part( 'loop-nav' );

		else :

			get_template_part( 'loop-no-posts' );

		endif;
		?>

	</div><!-- end of #content -->
</div>
<div id="menu">
<div class="menuHide">
</div>
<?php 
$landingPage = getLandingPage($post->ID);
echo do_shortcode('[wpv-post-body view_template="Left Side Menu" id="' . $landingPage . '"]');  ?> 
 <script type="text/javascript">
	openMenu(<?php echo $post->ID ?>);
	setLandingZone(<?php echo $landingPage ?>);
</script>	
</div>

<?php get_footer(); ?>
