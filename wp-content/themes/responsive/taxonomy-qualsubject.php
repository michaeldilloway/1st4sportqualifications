<?php

// Exit if accessed directly
if( !defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Archive Template
 *
 *
 * @file           archive.php
 * @package        Responsive
 * @author         Emil Uzelac
 * @copyright      2003 - 2014 CyberChimps
 * @license        license.txt
 * @version        Release: 1.1
 * @filesource     wp-content/themes/responsive/archive.php
 * @link           http://codex.wordpress.org/Theme_Development#Archive_.28archive.php.29
 * @since          available since Release 1.0
 */

get_header(); ?>
<?php $term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) ); ?>
<div id="singleContent">
	<?php 
	$shortcode = '[wpv-view name="subject-page-banner" subject="' . $term->slug . '" debug="true"]';
	echo do_shortcode($shortcode);
	
	//echo "Sport: " . $_GET['sport']
	?>

<div id="content-archive" class="<?php //echo implode( ' ', responsive_get_content_classes() ); ?>">

	<?php if( have_posts() ) : ?>
		<?php get_template_part( 'loop-header' ); ?>
		<h1><?php echo do_shortcode($term->name); ?></h1>
	<?php echo do_shortcode('[wpv-view name="sport-page-content" sport="' . $term->slug . '"]'); ?>
		<?php while( have_posts() ) : the_post(); ?>
			<?php responsive_entry_before(); ?>
			<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
				<?php responsive_entry_top(); ?>

				<?php //get_template_part( 'post-meta' ); ?>

				<div class="post-entry">
				    <?php the_excerpt(); ?>
					<?php //wp_link_pages( array( 'before' => '<div class="pagination">' . __( 'Pages:', 'responsive' ), 'after' => '</div>' ) ); ?>
				</div>
				<!-- end of .post-entry -->

				<?php get_template_part( 'post-data' ); ?>

				<?php responsive_entry_bottom(); ?>
			</div><!-- end of #post-<?php the_ID(); ?> -->
			<?php responsive_entry_after(); ?>

		<?php
		endwhile;
		get_template_part( 'loop-nav' );
	else :
		get_template_part( 'loop-no-posts' );
	endif;
	?>

</div><!-- end of #content-archive -->
</div>
<div id="menu">
<?php 
$landingPage = getLandingPage($post->ID);
echo do_shortcode('[wpv-post-body view_template="Left Side Menu" id="' . $landingPage . '"]');  ?> 
 <script type="text/javascript">
	openMenu(<?php echo $post->ID ?>);
	setLandingZone(<?php echo $landingPage ?>);
</script>	
</div>

<?php get_footer(); ?>
