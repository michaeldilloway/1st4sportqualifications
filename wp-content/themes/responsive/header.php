<?php

// Exit if accessed directly
if( !defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Header Template
 *
 *
 * @file           header.php
 * @package        Responsive
 * @author         Emil Uzelac
 * @copyright      2003 - 2014 CyberChimps
 * @license        license.txt
 * @version        Release: 1.3
 * @filesource     wp-content/themes/responsive/header.php
 * @link           http://codex.wordpress.org/Theme_Development#Document_Head_.28header.php.29
 * @since          available since Release 1.0
 */
?>
	<!doctype html>
	<!--[if !IE]>
	<html class="no-js non-ie" <?php language_attributes(); ?>> <![endif]-->
	<!--[if IE 7 ]>
	<html class="no-js ie7" <?php language_attributes(); ?>> <![endif]-->
	<!--[if IE 8 ]>
	<html class="no-js ie8" <?php language_attributes(); ?>> <![endif]-->
	<!--[if IE 9 ]>
	<html class="no-js ie9" <?php language_attributes(); ?>> <![endif]-->
	<!--[if gt IE 9]><!-->
<html class="no-js" <?php language_attributes(); ?>> <!--<![endif]-->
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=9" />
		<meta charset="<?php bloginfo( 'charset' ); ?>"/>
		<!--meta name="viewport" content="width=device-width, initial-scale=1.0"-->
		<meta name="viewport" content="width=device-width">
		<meta name="description" content="1st4sport Qualifications develop and award qualifications, in areas such as coaching and sports education, for the active leisure and learning industry.">
		<meta name="keywords" content="active leisure and learning, developing qualifications, awarding sports qualifications, sports coaching qualifications, qualification awarding body, football coaching qualifications, sports related apprenticeships, coaching apprenticeships, exercise and fitness instruction qualifications, sports NVQ">
		<link rel="icon" type="image/png" href="<?php echo home_url( '/' ); ?>img/icon/favicon.png" />
        <link rel="shortcut icon" href="<?php echo home_url( '/' ); ?>img/icon/favicon.ico" type="image/x-icon" />		
		<title><?php wp_title( '&#124;', true, 'right' ); ?></title>
		<link rel="profile" href="http://gmpg.org/xfn/11"/>
		<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>"/>
		
		<?php wp_head(); ?>
		<script type="text/javascript" src="<?php echo home_url( '/' ); ?>js/1st4sport.js"></script>
		<script type="text/javascript" src="<?php echo home_url( '/' ); ?>js/jquery.bxslider.min.js"></script>
		<script type="text/javascript" src="<?php echo home_url( '/' ); ?>js/jquery-ui.js"></script>
	</head>

<body <?php body_class(); ?>>
<?php echo '<!-- ' .  get_page_template()  . ' -->'; ?>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-3531123-1', 'auto');
  ga('send', 'pageview');

</script>
<?php responsive_container(); // before container hook ?>
<div id="container" class="hfeed">

<?php responsive_header(); // before header hook ?>
	
	<div id="header">
		<div id="headerContent">
			<?php responsive_header_top(); // before header content hook ?>
			<?php responsive_in_header(); // header hook ?>
			<div id="logo">
				<a href="<?php echo home_url( '/' ); ?>">
					<img src="<?php echo home_url( '/' ); ?>img/logo.gif" clas="logoImage" alt="<?php bloginfo( 'name' ); ?>"/>
				</a>
			</div><!-- end of #logo -->
			<div id="rightHeaderContainer">
				<div class="headerLinksContainer">
					<a class="linkHeader genBoxShadow" href="http://www.1st4sportqualifications.com/online-assessment/" title="Online Assessment"/>Online Assessment</a>
					<a class="linkHeader genBoxShadow" target="_blank" href="http://centreportal.1st4sportqualifications.com/" title="Centre Portal">Centre Portal</a>
					<a class="linkHeader genBoxShadow" href="<?php echo home_url( '/' ); ?>external_verifiers/athena-our-quality-assurance-system/" title="External Verifiers"/>Athena</a>					
				</div>
				<div class="searchContainer">
					<div class="search greyGradient genBoxShadow" >
						<form id="search" type="get" action="<?php echo home_url( '/' ); ?>">
							<input type="text" name="search"/>
							<a class="lupa" href='javascript:document.getElementById("search").submit()'>
								<img src="<?php echo home_url( '/' ); ?>img/icon/search-icon.png" alt="Search"/>
							</a>
						</form>
					</div>
				</div>
			</div>
		</div>
		<?php //get_sidebar( 'top' ); ?>
		<?php wp_nav_menu( array(
							   'container'       => 'div',
							   'container_class' => 'main-nav',
							   'fallback_cb'     => 'responsive_fallback_menu',
							   'theme_location'  => 'header-menu'
						   )
		);
		?>

		<?php responsive_header_bottom(); // after header content hook ?>

	</div><!-- end of #header -->
<?php responsive_header_end(); // after header container hook ?>

<?php responsive_wrapper(); // before wrapper container hook ?>
	<div id="wrapper" class="clearfix">

<?php responsive_wrapper_top(); // before wrapper content hook ?>
<?php responsive_in_wrapper(); // wrapper hook ?>