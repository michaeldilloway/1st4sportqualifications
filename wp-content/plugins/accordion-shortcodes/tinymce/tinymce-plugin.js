(function() {
	'use strict';
	
	tinymce.create('tinymce.plugins.accordionShortcodesExtensions', {
		init: function(editor, url) {
			editor.addButton('AccordionShortcode', {
				title: 'Add an accordion group',
				cmd: 'accordionShortcode',
				image: url + '/images/accordion.gif'
			});
			
			editor.addCommand('accordionShortcode', function() {
				var shortcode = '[accordion';
						
						
						
						
						shortcode += ']' + editor.selection.getContent() + '[/accordion]';
						
						editor.execCommand('mceInsertContent', 0, shortcode);
				// editor.windowManager.open({
					// title: 'Insert Accordion Shortcode',
					// body: [
						
					// ],
					// onsubmit: function(e) {
						// var shortcode = '[accordion';
						
						
						
						
						// shortcode += ']' + editor.selection.getContent() + '[/accordion]';
						
						// editor.execCommand('mceInsertContent', 0, shortcode);
					// }
				// });
			});
			
			// Accordion Item
			editor.addButton('AccordionItemShortcode', {
				title: 'Add an accordion item',
				cmd: 'accordionItemShortcode',
				image: url + '/images/accordion-item.gif'
			});
			
			editor.addCommand('accordionItemShortcode', function() {
				editor.windowManager.open({
					title: 'Insert Accordion Item Shortcode',
					body: [
						{
							type: 'textbox',
							name: 'title',
							label: 'Accordion Item Title',
							minWidth: 300
						},
						{
							type: 'textbox',
							name: 'id',
							label: 'ID (optional)',
							minWidth: 300
						},
						{
							type: 'container',
							html: 'If you want to set an anchor link to this specifig box set the text here'
						},
						{
							type: 'listbox',
							name: 'colour',
							label: 'Colour',
							minWidth: 75,
							values: [
								{text: 'Default (Dark Blue)', value: null},
								{text: 'Orange', value: 'orange'},
								{text: 'Red', value: 'red'},
								{text: 'Green', value: 'green'},
								{text: 'Light Blue', value: 'light-blue'}
							]
						},
						{
							type: 'container',
							html: 'Choose the colour code of the hidden box'
						}
					],
					onsubmit: function(e) {
						var shortcode = '[accordion-item title="';
						
						if (e.data.title) {
							shortcode += e.data.title;
						}
						shortcode += '"';
						
						if (e.data.id) {
							shortcode += ' id=' + e.data.id;
						}
						
						if (e.data.colour) {
							shortcode += ' colour=' + e.data.colour;
						}
						
						shortcode += ']' + editor.selection.getContent() + '[/accordion-item]';
						
						editor.execCommand('mceInsertContent', 0, shortcode);
					}
				});
			});
		},
		
		getInfo: function() {
			return {
				longname:  'Accordion Buttons',
				author:    'Phil Buchanan',
				authorurl: 'http://philbuchanan.com/',
				version:   '2.0'
			};
		}
	});
	
	tinymce.PluginManager.add('accordionShortcodesExtensions', tinymce.plugins.accordionShortcodesExtensions);
}());