function runPartnerSlider(){
		$("#partnersList").bxSlider({mode:"horizontal",
                       auto:false,
                       autoStart: false,
                       autoHover:true,
                       useCSS:false,
                       moveSlides: 1,
                       pause: 2000,
                       easing: "linear",
                       pager:false,
                       slideSelector:$(".contImgPartner"),
                       preloadImages:"all",
                       minSlides: 1,
                       maxSlides: 10,
                       slideWidth: "100px",
                       speed:1000,
					   slideHeight:"60px"
                       });
	}

function runMainSlider(){
	$("#slideList").bxSlider({mode:"fade",
                            auto:"true",
                            pager:false,
                            slideSelector:$(".contImgSlide"),
                            preloadImages:"all",
							pause:3000,
                            useCSS : false,
							responsive: false,
							touchEnabled: true});
	}


function openMenu(id, notfirst){
	var $item = $("#menu").find("div[locator='" + id + "']");
	if (!$item.length ) {
		return;
	}
	if (notfirst == null || notfirst == undefined ){
		$item.children(".itemLeftMenu").addClass("lightBlue");
	}
	$item.children(".blockLeftMenu").addClass("visible");
	if ($item.children(".blockLeftMenu").html().trim() !=""){
		$item.children(".itemLeftMenu:not(#menu>div.itemLeftMenuCont>div.itemLeftMenu)").addClass("noBorder");
	}

	if ($item.parent().parent(".itemLeftMenuCont[locator]").attr("locator")){
		openMenu($item.parent().parent(".itemLeftMenuCont[locator]").attr("locator"), true);
	}
}

function setLandingZone(idLanding){
	$("#menu-item-" + idLanding).addClass("current_page_item");
}

function initialiseAccordion(){
	$(".accordion .acctitle").click(function(){
		var itemFirst = $(this);
		var item = $(this).next(".acccontent");
		if (item.is(":visible")){
			itemFirst.removeClass("selected");
			item.hide();
		}else{
			itemFirst.addClass("selected");
			item.show();
		}
	});
}

$(document).ready(function(){
	runMainSlider();
	runPartnerSlider();
	$("#homeLinks").change(function(){
		if ($(this).val() != "") {
			if ($(this).find(":selected").attr("newWindow") == "1"){
				window.open($(this).val());
			}else{
				$(location).attr("href", $(this).val());
			}
		}
	});
	$(".qualificationSearchTitle").click(function(){
		$(".qualificationSearchTitle").removeClass("searchSelected");
		$(".qualificationSearchContent").slideUp(300);
		$(this).addClass("searchSelected").parent().find(".qualificationSearchContent").slideDown(300);
	});
	if ($(".qualificationSearchTitle").length){
		$(".qualificationSearchTitle")[0].click();
	}

	initialiseAccordion();

	if ($(".rightHand").length != 0){
		$(".responsiveRight").html($(".rightHand").html());
	}else{
		$(".responsiveRight").remove();
	}

	$(".tabs table tr:first-of-type td:first-of-type[colspan='2']").parents("table").addClass("struct");

	$(".qualificationSearchContent form").submit(function(){
	  var result = true;
	  $(this).find("input[type='text']").each(function(e,i){
		if ($(this).val()==""){
		  result = false;
		}
	  });
	  return result;
	});

	$(".menuHide").click(function(){
		$("#menu").animate({left: "-100%"}, 400);
		$(".menuShow").animate({left: "-20px"}, 400);
	});
	$(".menuShow").click(function(){
		$("#menu").animate({left: "-20px"}, 400);
		$(".menuShow").animate({left: "-100%"}, 400);
	});

	$(".acccontainer").each(function(){
	  if ($(this).find(".acccontent").find(".accBox").length == 0){
		   $(this).remove();
	  }
	});

	$(".qualsList .accBox").each(function(){
	  if ($(this).find("a").length == 0){
		   $(this).remove();
	  }
	});

	checkAnchor();

	$(".tabs").tabs({ hide : {effect:"slideUp", duration:200}	, show: { effect:"slideDown", duration: 200}} ).show();

	$(".centre-qual table:not(.tabs table)").addClass("struct").wrap("<div class='tabs' style='display:visible'></div>").parent().show();
	$(".learner-qual table:not(.tabs table)").addClass("struct").wrap("<div class='tabs' style='display:visible'></div>").parent().show();
	$("body").show();

});

function checkAnchor(){
	if (!(window.location.href == null || window.location.href == "") && window.location.href.indexOf("#") != -1){
		var subject = window.location.href.substring(window.location.href.indexOf("#")+1);
		$(".accordion .acctitle." + subject ).click();
		if ($(".accordion .acctitle.coaching").parent().find(".acctitle." + subject).length != 0){
			$(".accordion .acctitle.coaching").click();
		}
		goToAnchor(subject);
	}else{
		$(".accordion .acctitle.coaching").click();
	}
}

function goToAnchor(anchor){
	if ($('#' + anchor).length !=0){
		$("body,html").animate({
			scrollTop:   $('#' + anchor).offset().top
		}, 1000);
	}
}

/*
Centre Finder
*/
var geocoder;
var cfMap;
var infowindow;
var cfMarkers = [];

$(document).ready(function() {
	if ($("#mapCanvas").length !=0){
		// Async Load Google Maps v3 API
		$("#subject").change(function(){
				if ($("#subject").val() =="Coaching"){
					$(".finderSelSport").show();
				}else{
					$(".finderSelSport").hide();
				}
			});

		$("#subject").change();
		var script = document.createElement("script");
		script.type = "text/javascript";
		script.src = "https://maps.googleapis.com/maps/api/js?sensor=false&callback=cfInitialiseMap";
		document.body.appendChild(script);

		$("#find-a-centre-search").click(function(){
			cfPerformSearch(true);
		});
	}
});

function cfInitialiseMap() {
    geocoder = new google.maps.Geocoder();
    var mapOptions = {
        zoom: 5,
        center: new google.maps.LatLng(54.8924730, -2.9329310), // Carlisle
        mapTypeId: google.maps.MapTypeId.ROADMAP
    }
    cfMap = new google.maps.Map(document.getElementById("mapCanvas"), mapOptions);

    infowindow = new google.maps.InfoWindow({
        content: "",
		maxWidth: 280

    });

	var param = $.urlParam("address");
	if (param != null && param !=""){
		$("#address").val(decodeURIComponent(param).replace(/\+/g, " "));
		$("#distance").val(10);
		cfPerformSearch(false);
	}

}


function loadQuals(){
	$.ajax({
				url: "/qualifications-ajax/",
				cache: true,
				async: true,
				success: function(data) {
					$("#fullquals").html(data);
				},
				beforeSend: function(){
					$("#loadingQuals").show();
				},
				error: function (jqXHR, textStatus, errorThrown) {
					$("#fullquals").html("Sorry we couldn't load the qualifications");
				},
				complete: function(){
					$("#loadingQuals").hide();
					initialiseAccordion();
					checkAnchor();
				}
			});
}

function cfPerformSearch(performValidation) {
    $('.notFoundMessage').hide();
    $("#secondDisclaimer").hide();
	 var address = $("#address").val();
        var range = $("#distance").val();
        var sport = "";
		var subject = encodeURIComponent($("#subject").val());
		if (subject == 'Coaching'){
			sport = encodeURIComponent($("#sport").val());
		}

		if (performValidation == true) {
			var classes = [];
			if (subject ==""){
				classes.push(".subjectErr");
			}else if (subject =="Coaching" && sport==""){
				classes.push(".sportErr");
			}
			if (address==""){
				classes.push(".addressErr");
			}

			if (classes.length !=0){
				$(".errorFields ul").show().find("li").hide();
				$(classes.join(",")).show();
				return;
			}else{
				$(".errorFields ul").hide().find("li").hide();
			}

		}
		var lati =0;
		var longi = 0;
		var geocoder = new google.maps.Geocoder();
		geocoder.geocode({
			"address": address
		}, function(results) {
			lati = results[0].geometry.location.lat();
			longi = results[0].geometry.location.lng() ;


			var searchUrl = "/find-a-centre-ajax/?address=" + address + "&distance=" + range + "&subjectFind=" + subject + "&sportFind=" + sport + "&lati=" + lati.toString()  + "&longi=" + longi.toString()  ;
			console.log(searchUrl);
			$.ajax({
				url: searchUrl,
				cache: false,
				success: function(data) {
					var d = $.parseJSON(data)
					setResults(d.FindCentresResult, range);
				},
				beforeSend: function(){
					$("#finderResults").html("");
					$("#noResults").hide();
					$("#loading").show();
				},
				 error: function (jqXHR, textStatus, errorThrown) {
						   //alert(errorThrown);
						},
				complete: function(){
					$("#loading").hide();
				}
			});

		});



}

function setResults(result, distance){
for (var i = 0; i < cfMarkers.length; i++) {
			cfMarkers[i].setMap(null);
		}
cfMarkers = [];

if (result.Centres.length == 0){
	$("#noResults").show();
	return;
}
$("#noResults").hide();
	for (var i =0; i<result.Centres.length;i++){
	var centre = result.Centres[i];
    var marker = new google.maps.Marker({ map: cfMap, position: new google.maps.LatLng(centre.Coordinates.latitude, centre.Coordinates.longitude) });
                                 cfMarkers.push(marker);
                                 marker.setTitle(centre.Name);
                                 marker.setClickable(true);
                                 marker.html = "<div class='marker'><span class='markerTitle'>" + centre.Name + "</span><ul>";
								 for (var j = 0; j < centre.Qualifications.length; j++) {
									var qualification = centre.Qualifications[j];
									var URLqual = "";
									if (qualification.Slug !=undefined && qualification.Slug !="") {
										URLqual += "<a href='http://www.1st4sportqualifications.com/candidate_information/qualifications/qualificationbin/" + qualification.Slug + "'>";
                                        URLqual += qualification.Title;
                                        URLqual += "</a>";
									}else{
										URLqual += qualification.Title;
									}
									var htmlResults = "<div class='results'><span class='title'>" + URLqual + "</span>";
									htmlResults += "<div>" + centre.Name + " (" + centre.Distance + " miles)</div>"
									htmlResults += "<div>" + centre.Address + "</div>"
									if (centre.Website != null && centre.Website != ""){
										var url = "";
										if (centre.Website.indexOf('http\:\/\/') == -1) {
											url = "http://" + centre.Website;
										}else{
											url =centre.Website;
										}
										htmlResults += "<div>website: <a target='_blank' href='" + centre.Website + "'>" + centre.Website + "</a></div>"
									}
									if (centre.Email != null && centre.Email != ""){
										htmlResults += "<div>email: <a href='mailto:" + centre.Email + "'>" + centre.Email + "</a></div>"
									}
									$("#finderResults").append(htmlResults);

									marker.html += "<li>";
									marker.html += URLqual;
									marker.html += "</li>";
								 }

                                 marker.html = marker.html + "</ul></div>";
								 marker.setMap(cfMap);
                                 google.maps.event.addListener(marker, 'click', function() {
                                     infowindow.setContent(this.html);
									 //cfMap.setCenter(marker.getPosition());
                                     infowindow.open(cfMap, this);
                                 });
	}
	//Later
	cfMap.setCenter(new google.maps.LatLng(result.Coordinates.latitude, result.Coordinates.longitude));
	cfMap.setZoom(getZoomLevelFromRange(distance));
}

function getZoomLevelFromRange(range) {
    switch (range) {
        case "5": return 11;
        case "10": return 10;
        case "25": return 9;
        case "50": return 8;
        case "100": return 7;
        default: return 5;
    }
}

function cfHandleEnter() {
    if ((event.which && event.which == 13) || (event.keyCode && event.keyCode == 13)) {
        cfPerformSearch(true);
        if (!e) {
            var e = window.event;
        }
        e.cancelBubble = true;
        e.returnValue = false;
        if (e.stopPropagation) {
            e.stopPropagation();
            e.preventDefault();
        }
    }
    return false;
}

	$.urlParam = function(name){
	if (!(window.location.href == null || window.location.href == "")){
		var results = new RegExp('[\?&;]' + name + '=([^&#]*)').exec(window.location.href);
		if (results == null){
			return "";
		}else{
			return results[1] || 0;
		}
	}

}
